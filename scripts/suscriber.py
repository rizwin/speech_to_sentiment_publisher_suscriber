import rospy
from std_msgs.msg import String
#three lists are defined
word1=["hello","good","with pleasure"]
word2=["bad","mad","sad"]
word3=["okay"]

def callback(data):
    
    io=data.data
    if io in word1:
                               
                               rospy.loginfo(rospy.get_caller_id() + "--%s--you sound positive:)", data.data) #prints the data
                               
    elif io in word2:
                               rospy.loginfo(rospy.get_caller_id() + "--%s--you sound negative:(", data.data)
                               
                              
    elif io in word3: 
                               rospy.loginfo(rospy.get_caller_id() + "--%s--you sound neutral:]", data.data)
                               
                              
    else:
                               rospy.loginfo(rospy.get_caller_id() + "--%s--You are emotionless!!", data.data)
                              
    
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber("speech_recognition", String, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()

