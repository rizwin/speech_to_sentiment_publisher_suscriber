#Python 2.x program to transcribe an Audio file using google API
#An object sr of speech recognition is imported.You have to install speech recognition in order to import it
#os is imported so that it allows the integration of linux commands in python
#python packages for rospy is imported
import speech_recognition as sr
import os
import rospy
from std_msgs.msg import String
#publisher is deifined.here speech_recognition is the topic name,talker is the node's name
pub = rospy.Publisher('speech_recognition', String, queue_size=10)
rospy.init_node('talker', anonymous=True)
rate = rospy.Rate(10) # 10hz

def speech_to_text():
    
    while not rospy.is_shutdown():
        os.system("ffmpeg -f pulse -i default speech_to_text.wav")  #this command records the audio and saves it to speech_to_text.wav"
        print "Google is recognising audio please wait...."
        AUDIO_FILE = ("speech_to_text.wav")
        # use the audio file as the audio source
        r = sr.Recognizer()
 
        with sr.AudioFile(AUDIO_FILE) as source:
    #reads the audio file. Here we use record instead of listen
             audio = r.record(source)  
 
        try:
             k=r.recognize_google(audio)  #transcribed word is stored in k
             rospy.loginfo(k)             #make the output as screen
             pub.publish(k)               #publishes the data
             rate.sleep()                 #keeps the loop running
  
        except sr.UnknownValueError:
             c="Google Speech Recognition could not understand audio"
             rospy.loginfo(c)
             pub.publish(c)
             rate.sleep()
 
        except sr.RequestError as e:
             print("Could not request results from Google Speech Recognition service; {0}".format(e))
           

if __name__ == '__main__':
    try:
        speech_to_text()
    except rospy.ROSInterruptException:
        pass
